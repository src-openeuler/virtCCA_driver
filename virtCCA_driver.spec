# spec file for package virtCCA_driver
#
# Copyright (c) Huawei Technologies Co., Ltd. 2024. All rights reserved.
#
Name         :  virtCCA_driver
Summary      :  virtCCA driver is some drivers for TEE specific function.
Version      :  0.1.8
Release      :  1
ExclusiveArch:  aarch64
License      :  GPLV2
Group        :  System/Kernel
URL          :  https://gitee.com/openeuler/virtCCA_driver.git
Source0      :  https://gitee.com/openeuler/virtCCA_driver/repository/archive/%{name}-v%{version}.tar.gz
BuildRoot    :  %{_tmppath}/%{name}-v%{release}-build
BuildRequires:	gcc kernel-devel kernel-headers kernel glibc glibc-devel glib2-devel cmake rpm
Requires     :  kmod

%global debug_package   %{nil}
%define kmod_1_name     tmm_driver
%define kmod_2_name     sealing_key
%define kmod_3_name     kae_driver
%define kernel          %(ver=`rpm -qa|grep kernel-devel`;echo ${ver#*kernel-devel-})

%description
%{name} module

%prep
%autosetup -n %{name}-v%{version}

%build
cd %_builddir/%{name}-v%{version}/
make KERNEL_DIR=/usr/src/kernels/%{kernel}

%install
mkdir -p %{buildroot}/lib/modules/%{kernel}/extra
install -m 0640 %_builddir/%{name}-v%{version}/%{kmod_1_name}/src/%{kmod_1_name}.ko        %{buildroot}/lib/modules/%{kernel}/extra
install -m 0640 %_builddir/%{name}-v%{version}/%{kmod_2_name}/src/%{kmod_2_name}.ko        %{buildroot}/lib/modules/%{kernel}/extra
install -m 0640 %_builddir/%{name}-v%{version}/%{kmod_3_name}/hisi_plat_qm.ko        %{buildroot}/lib/modules/%{kernel}/extra
install -m 0640 %_builddir/%{name}-v%{version}/%{kmod_3_name}/hisi_plat_sec.ko        %{buildroot}/lib/modules/%{kernel}/extra
install -m 0640 %_builddir/%{name}-v%{version}/%{kmod_3_name}/hisi_plat_hpre.ko        %{buildroot}/lib/modules/%{kernel}/extra

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%attr(0640,root,root) /lib/modules/%{kernel}/extra/*

%post
if [[ "$1" = "1" || "$1" = "2" ]] ; then
    echo "installing virtCCA driver"
    install_dir="/lib/modules/%{kernel}/extra"
    link_dir="/lib/modules/$(uname -r)/extra"
    if [ "$install_dir" != "$link_dir" ] ; then
        mkdir -p $link_dir
        ln -sf "$install_dir/%{kmod_1_name}.ko"  "$link_dir/%{kmod_1_name}.ko"
        ln -sf "$install_dir/%{kmod_2_name}.ko"  "$link_dir/%{kmod_2_name}.ko"
        ln -sf "$install_dir/hisi_plat_qm.ko"  "$link_dir/hisi_plat_qm.ko"
        ln -sf "$install_dir/hisi_plat_sec.ko"  "$link_dir/hisi_plat_sec.ko"
        ln -sf "$install_dir/hisi_plat_hpre.ko"  "$link_dir/hisi_plat_hpre.ko"
    fi
    echo "installed virtCCA driver"
fi

%postun
if [[ "$1" = "0" ]] ; then
    echo "removing virtCCA driver"
    link_dir="/lib/modules/$(uname -r)/extra"
    install_dir="/lib/modules/%{kernel}/extra"
    if [ "$install_dir" != "$link_dir" ] ; then
        rm -rf "$link_dir/%{kmod_1_name}.ko"
        rm -rf "$link_dir/%{kmod_2_name}.ko"
        rm -rf "$link_dir/hisi_plat_qm.ko"
        rm -rf "$link_dir/hisi_plat_sec.ko"
        rm -rf "$link_dir/hisi_plat_hpre.ko"
    fi
    echo "removed virtCCA driver"
fi

%changelog
* Mon Dec 2 2024 chenzheng<chenzheng71@huawei.com> - 0.1.8-1
- Type:bugfix
- DESC:fix compile error and change sealing key interface parameter for avoid ambiguity

* Mon Nov 11 2024 yuzexi<yuzexi@hisilicon.com> - 0.1.6-1
- Type:bugfix
- DESC:fix compile error

* Thu Nov 7 2024 yuzexi<yuzexi@hisilicon.com> - 0.1.5-1
- Type:bugfix
- DESC:modify platform driver according to comments

* Tue Sep 10 2024 yuzexi<yuzexi@hisilicon.com> - 0.1.4-1
- Type:enhancement
- DESC:Add kae driver for virtCCA guest

* Tue Sep 10 2024 chenzheng<chenzheng71@huawei.com> - 0.1.3-7
- Type:bugfix
- DESC:mkdir extra dir while kernel version not equal

* Mon Sep 9 2024 chenzheng<chenzheng71@huawei.com> - 0.1.3-6
- Type:bugfix
- DESC:weak-modules cannot create soft link in cvm

* Mon Jul 15 2024 tujipei<tujipei@huawei.com> - 0.1.3-5
- Type:bugfix
- DESC:Use weak-modules to decouple kernel modules from kernel versions

* Mon Jun 3 2024 tujipei<tujipei@huawei.com> - 0.1.3-4
- Type:bugfix
- DESC:Clean the scanned security problem for the sealing key code

* Mon Jun 3 2024 tujipei<tujipei@huawei.com> - 0.1.2-3
- Type:bugfix
- DESC:Fit class_create for different kernel version

* Fri May 31 2024 tujipei<tujipei@huawei.com> - 0.1.1-2
- Type:enhancement
- DESC:Add sealing key driver for kernel
- Type:enhancement
- DESC:chang build architecture

* Tue May 28 2024 tujipei<tujipei@huawei.com> - 0.1.0-1
- DESC:init virtCCA_driver
